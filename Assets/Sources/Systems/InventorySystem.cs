using Entitas;
using Entitas.Unity;
using UnityEngine;



public sealed class InventorySystem : IInitializeSystem
{
    [SerializeField]
    private GameSettings _gameSettings;

    GameObject _gameController;
    private readonly Contexts _contexts;



    public InventorySystem(Contexts contexts,GameObject GameController)
    {
        _contexts = contexts;
        _gameController = GameController;
    }

    public void Initialize()
    {
        _gameSettings = Resources.Load<GameSettings>("GameSettings");
        var inventory =_contexts.game.CreateEntity();
        inventory.AddInventory(_gameSettings.Shovels(),0);
        _gameController.Link(inventory);
    }
}