using Entitas;
using System.Collections.Generic;
using UnityEngine;

public sealed class TileColorSystem : ReactiveSystem<GameEntity>
{
    public TileColorSystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Level, GameMatcher.Gold));
    }

    protected override bool Filter(GameEntity entity)
    {
        //return entity.hasLevel;
        return entity.hasLevel;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            if (e.level.value > 0)
            {
                e.ReplaceColor(new Color(0, 255, 0, 255));
            }
            else
            {
                e.ReplaceColor(new Color(0, 0, 0, 1));
            }

            e.tile.tile.GetComponent<SpriteRenderer>().color = e.color.color;
        }
    }
}