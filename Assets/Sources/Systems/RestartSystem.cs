using Entitas;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using Entitas.Unity;

public sealed class RestartSystem : ReactiveSystem<GameEntity>
{
    Contexts _contexts;

    public RestartSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        /*return new Collector<GameEntity>(
            new[] {context.GetGroup(GameMatcher.Level), context.GetGroup(GameMatcher.Inventory)},
            new[] {GroupEvent.Added, GroupEvent.Added});*/
        return context.CreateCollector(GameMatcher.Level);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasLevel;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            e.tile.tile.Unlink();
        }

        SceneManager.LoadScene(0);
    }
}