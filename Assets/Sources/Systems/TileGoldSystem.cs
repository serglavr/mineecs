using Entitas;
using System.Collections.Generic;
using UnityEngine;

public sealed class TileGoldSystem : ReactiveSystem<GameEntity>
{
    
    public TileGoldSystem(Contexts contexts) : base(contexts.game)
    {
        
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        //  return context.CreateCollector(GameMatcher.Gold);
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Level, GameMatcher.Gold));
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasGold;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            if (e.level.value > 0)
            {
                if (e.gold.isGold)
                {
                    e.ReplaceColor(new Color(255, 210, 0, 255));
                }
                else
                {
                    e.ReplaceColor(new Color(0, 255, 0, 255));
                }
                e.tile.tile.GetComponent<SpriteRenderer>().color = e.color.color;
            }
        }
    }
}