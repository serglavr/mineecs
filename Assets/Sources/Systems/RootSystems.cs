using UnityEngine;

public sealed class RootSystems : Feature
{
    public RootSystems(Contexts contexts, GameObject tilePrefab)
    {
        Add(new CreateTileSystem(contexts, tilePrefab));
        Add(new LogLevelSystem(contexts));
        Add(new TileColorSystem(contexts));
        Add(new TileGoldSystem(contexts));
        Add(new ClickTileSystem(contexts));
        Add(new InventorySystem(contexts, GameObject.Find("Game Controller")));
        Add(new UIShovelsSystem(contexts));
        Add(new UIGoldSystem(contexts));
        Add(new UILoseSystem(contexts));
        Add(new UIWinSystem(contexts));
    }
}