using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public sealed class ClickTileSystem : ReactiveSystem<GameEntity>
{
    GameObject _gameController = GameObject.Find("Game Controller");
    IEntity _target;
    GameSettings _gameSettings = Resources.Load<GameSettings>("GameSettings");
    private bool golden;

    public ClickTileSystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Clicked);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasClicked;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            /*if (e.clicked.isClicked)
            {
                // Debug.Log("Clicked: " + e.creationIndex);
                TileClick(e);
                ClickedComponent clickedComponent = ((ClickedComponent)e.GetComponent(GameComponentsLookup.Clicked));
                clickedComponent.isClicked = false;
                e.ReplaceComponent(GameComponentsLookup.Clicked, clickedComponent);
            }*/
            TileClick(e);
            e.RemoveClicked();
        }
    }

    void TileClick(IEntity ent)
    {
        var invTarget = _gameController.GetEntityLink().entity;
        InventoryComponent inventorycomponent =
            ((InventoryComponent)invTarget.GetComponent(GameComponentsLookup.Inventory));
        var target = ent;
        LevelComponent levelcomponent = ((LevelComponent)target.GetComponent(GameComponentsLookup.Level));
        GoldComponent goldComponent = ((GoldComponent)target.GetComponent(GameComponentsLookup.Gold));
        if (inventorycomponent.Shovels > 0 && inventorycomponent.Gold < _gameSettings.GoldMax())
        {
            ChangeLevel(levelcomponent, goldComponent, target);
        }
    }

    void InvRefresh(bool golden)
    {
        _target = _gameController.GetEntityLink().entity;
        InventoryComponent inventorycomponent =
            ((InventoryComponent)_target.GetComponent(GameComponentsLookup.Inventory));
        if (!golden)
        {
            if (inventorycomponent.Shovels > 0)
            {
                inventorycomponent.Shovels--;
                _target.ReplaceComponent(GameComponentsLookup.Inventory, inventorycomponent);
            }
        }
        else
        {
            {
                if (inventorycomponent.Gold < _gameSettings.GoldMax())
                {
                    inventorycomponent.Gold++;
                    _target.ReplaceComponent(GameComponentsLookup.Inventory, inventorycomponent);
                }
            }
        }
    }

    void ChangeLevel(LevelComponent levelcomponent, GoldComponent goldComponent,
        IEntity target)
    {
        if (levelcomponent.value > 0)
        {
            golden = goldComponent.isGold;
            if (!goldComponent.isGold)
            {
                levelcomponent.value--;
                target.ReplaceComponent(GameComponentsLookup.Level, levelcomponent);
                MakeTileGold(goldComponent);
                target.ReplaceComponent(GameComponentsLookup.Gold, goldComponent);
                InvRefresh(golden);
            }
            else
            {
                goldComponent.isGold = false;
                target.ReplaceComponent(GameComponentsLookup.Gold, goldComponent);
                InvRefresh(golden);
            }
        }
    }

    void MakeTileGold(GoldComponent goldComponent)
    {
        int goldRand = Random.Range(0, 101);
        if (goldRand < _gameSettings.GoldChance())
        {
            goldComponent.isGold = true;
        }
    }
}