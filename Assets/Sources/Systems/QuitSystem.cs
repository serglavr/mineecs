using Entitas;
using UnityEngine;

public sealed class QuitSystem : IExecuteSystem
{
    Contexts _contexts;

    public QuitSystem(Contexts contexts)
    {
        _contexts = contexts;
    }

    public void Execute()
    {
        Application.Quit();
    }
}