using Entitas;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public sealed class UIShovelsSystem : ReactiveSystem<GameEntity>,IInitializeSystem
{
    public TextMeshProUGUI UI_Shovels;
    [SerializeField]
    private GameSettings _gameSettings;
    public UIShovelsSystem(Contexts contexts) : base(contexts.game)
    {
        
    }

    public void Initialize()
    {
        _gameSettings = Resources.Load<GameSettings>("GameSettings");
        var canvas = GameObject.Find("Canvas");
        UI_Shovels = canvas.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        UI_Shovels.text = "Shovels: "+_gameSettings.Shovels().ToString();
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Inventory);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasInventory;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            UI_Shovels.text = "Shovels: " + e.inventory.Shovels.ToString();
            //    UnityEngine.Debug.Log("Level: "+level);
        }
    }
}