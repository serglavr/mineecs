using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

public sealed class CreateTileSystem : IInitializeSystem
{
    public GameObject _tilePrefab;
    private readonly Contexts _contexts;
    [SerializeField] private GameSettings _gameSettings;

    int _levels;
    int _numOfTiles;
    float _distBtwTiles;
    int _tilesPerRow;
    int _startPosX;
    int _startPosY;
    SpriteRenderer m_SpriteRenderer;
    public List<Tile> TileList;
    private TilesController _tilesController;

    public CreateTileSystem(Contexts contexts, GameObject tile)
    {
        _contexts = contexts;
        _tilePrefab = tile;
    }

    public void Initialize()
    {
        _gameSettings = Resources.Load<GameSettings>("GameSettings");
        _levels = _gameSettings.TileLevels();
        _numOfTiles = _gameSettings.NumOfTiles();
        _distBtwTiles = _gameSettings.DistBtwTiles();
        _tilesPerRow = _gameSettings.TilesPerRow();
        _startPosX = _gameSettings.StartPosX();
        _startPosY = _gameSettings.StartPosY();
        TileList = new List<Tile>();
        // _ClickTileSystem = new ClickTileSystem(_contexts);
        // _ClickTileSystem.Initialize();
        CreateTiles(_numOfTiles, _distBtwTiles, _tilesPerRow, _levels);
    }

    void CreateTiles(int NumOfTiles, float DistBtwTiles, int TilesPerRow, int Levels)
    {
        float xOffset = 0.0f;
        float yOffset = 0.0f;

        for (int tilesCreated = 0; tilesCreated < NumOfTiles; tilesCreated++)
        {
            xOffset += DistBtwTiles;
            if (tilesCreated % TilesPerRow == 0)
            {
                yOffset += DistBtwTiles;
                xOffset = 0;
            }

            var tile = GameObject.Instantiate(_tilePrefab, new Vector2(_startPosX + xOffset, _startPosY + yOffset),
                Quaternion.identity);
            var e = _contexts.game.CreateEntity();
            e.AddTile(tile);
            tile.Link(e);
            e.AddLevel(Levels);
            e.AddGold(false);
            e.AddColor(new Color(0, 255, 0, 255));
            // e.AddClicked(false);
            tile.GetComponent<SpriteRenderer>().color = e.color.color;
            tile.GetComponent<Tile>().SetIndex(tilesCreated);
            TileList.Add(tile.GetComponent<Tile>());
        }

        Tile[] TArray = TileList.ToArray();
        _tilesController = new TilesController(TArray);
    }
}