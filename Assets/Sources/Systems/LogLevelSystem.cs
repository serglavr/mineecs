using Entitas;
using System.Collections.Generic;

public sealed class LogLevelSystem : ReactiveSystem<GameEntity>
{
    public LogLevelSystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Level);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasLevel;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            var level = e.level.value;
            //    UnityEngine.Debug.Log("Level: "+level);
        }
    }
}


/*public sealed class LogLevelSystem : IExecuteSystem
{
    readonly IGroup<GameEntity> _entities;

    public LogLevelSystem(Contexts contexts)
    {
        _entities = contexts.game.GetGroup(GameMatcher.Level);
    }

     public void Execute()
     {
         foreach (var e in _entities)
         {
             var level = e.level.value;
             UnityEngine.Debug.Log("Level: "+level);
         }
     }
}*/