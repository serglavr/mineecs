using Entitas;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public sealed class UIGoldSystem : ReactiveSystem<GameEntity>,IInitializeSystem
{
    public TextMeshProUGUI UI_Gold;
    [SerializeField]
    private GameSettings _gameSettings;
    public UIGoldSystem(Contexts contexts) : base(contexts.game)
    {
        
    }

    public void Initialize()
    {
        int start = 0;
        var canvas = GameObject.Find("Canvas");
        UI_Gold = canvas.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        UI_Gold.text = "Gold: "+start.ToString();
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Inventory);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasInventory;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            UI_Gold.text = "Gold: " + e.inventory.Gold.ToString();
            //    UnityEngine.Debug.Log("Level: "+level);
        }
    }
}