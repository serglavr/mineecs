using Entitas;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public sealed class UILoseSystem : ReactiveSystem<GameEntity>,IInitializeSystem
{
    public GameObject GameOver;
    [SerializeField]
    private GameSettings _gameSettings;
    public UILoseSystem(Contexts contexts) : base(contexts.game)
    {
        
    }

    public void Initialize()
    {
        _gameSettings = Resources.Load<GameSettings>("GameSettings");
        var canvas = GameObject.Find("Canvas");
        GameOver = canvas.transform.Find("GameOver").gameObject;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Inventory);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasInventory;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            if(e.inventory.Shovels == 0)
                GameOver.SetActive(true);
            //    UnityEngine.Debug.Log("Level: "+level);
        }
    }
}