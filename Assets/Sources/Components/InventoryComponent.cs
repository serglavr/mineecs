using Entitas;

public sealed class InventoryComponent : IComponent
{
    public int Shovels;
    public int Gold;
}