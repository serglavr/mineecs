using Entitas;
using UnityEngine;

public sealed class TileComponent : IComponent
{
    public GameObject tile;
}