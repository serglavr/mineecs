using Entitas;
using UnityEngine;

public sealed class PositionComponent : IComponent
{
    public Vector2 value;
}