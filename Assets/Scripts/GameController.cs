using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    RootSystems _systems;
    RestartSystem _restartSystem;
    QuitSystem _quitSystem;
    public GameObject _tilePrefab;

    // Start is called before the first frame update
    void Start()
    {
        //   var contexts = new Contexts();
        var contexts = Contexts.sharedInstance;
        _systems = new RootSystems(contexts, _tilePrefab);
        _systems.Initialize();
        _restartSystem = new RestartSystem(contexts);
        _quitSystem = new QuitSystem(contexts);
    }

    // Update is called once per frame
    void Update()
    {
        _systems.Execute();
        if (Input.GetKeyDown(KeyCode.R))
        {
            _restartSystem.Execute();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _quitSystem.Execute();
        }
    }
}