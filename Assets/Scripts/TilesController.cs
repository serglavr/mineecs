using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using Entitas.Unity;

public class TilesController
{
    private Tile[] _Tiles;

    public TilesController(Tile[] view)
    {
        _Tiles = view;
        foreach (Tile v in _Tiles)
        {
            v.TileClick += this.TileClick;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void TileClick(GameObject go)
    {
        var target = go.GetEntityLink().entity;
        if (!target.HasComponent(GameComponentsLookup.Clicked))
        {
            target.AddComponent(GameComponentsLookup.Clicked, new ClickedComponent());
        }
        //   ClickedComponent clickedComponent = ((ClickedComponent)target.GetComponent(GameComponentsLookup.Clicked));
        //   clickedComponent.isClicked = true;
        //   target.ReplaceComponent(GameComponentsLookup.Clicked, clickedComponent);
    }
}