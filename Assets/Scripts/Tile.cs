using UnityEngine;

public class Tile : MonoBehaviour
{
    public int Index;

    public delegate void TileEvent(GameObject entity);

    public TileEvent TileClick;

    // Start is called before the first frame update
    public void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnMouseDown()
    {
        TileClick?.Invoke(gameObject);
    }

    public void SetIndex(int ind)
    {
        Index = ind;
    }
}