using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "GameSettings", order = 51)]
public class GameSettings : ScriptableObject
{
    [SerializeField]
    private int goldMax;
    [SerializeField]
    private int goldChance;
    [SerializeField]
    private int tileLevels;
    [SerializeField]
    private int shovels;
    [SerializeField]
    private int numOfTiles;
    [SerializeField]
    private float distBtwTiles;
    [SerializeField]
    private int tilesPerRow;
    [SerializeField]
    private int startPosX;
    [SerializeField]
    private int startPosY;

    public int GoldMax()
    {
        return goldMax;
    }
    public int GoldChance()
    {
        return goldChance;
    }
    public int TileLevels()
    {
        return tileLevels;
    }
    public int Shovels()
    {
        return shovels;
    }
    public int NumOfTiles()
    {
        return numOfTiles;
    }
    public float DistBtwTiles()
    {
        return distBtwTiles;
    }
    public int TilesPerRow()
    {
        return tilesPerRow;
    }

    public int StartPosX()
    {
        return startPosX;
    }
    public int StartPosY()
    {
        return startPosY;
    }
}